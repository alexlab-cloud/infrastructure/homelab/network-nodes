# Network Node Data

This project contains some simple configuration for each of the nodes I manage on my home network.

Keeping these assets and other data in GitLab publicly makes it readily accessible over the internet,
so I can just e.g. `wget <resource>` to have access to it on a home server.
